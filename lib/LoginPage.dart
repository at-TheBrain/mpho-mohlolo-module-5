// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'DashBoard.dart';
import 'RegistrationPage.dart';
import 'TextField.dart';
import 'button.dart';
import 'package:firebase_auth/firebase_auth.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  //Sign Method

  Future signIn() async {
    await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: _emailController.text.trim(),
        password: _passwordController.text.trim());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //Set Up scaffold appearance
        backgroundColor: Colors.grey[400],
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/Zodiac/try1.gif'),
                  fit: BoxFit.cover)),
          child: SafeArea(
              child: Center(
                  child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                //Welcome text

                Text(
                  'Dear Pisces',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 34,
                    color: Colors.white,
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  'Welcome!',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 24,
                      color: Colors.blueGrey,
                      fontFamily: 'IndieFlower'),
                ),

                //Space Betweeen
                SizedBox(height: 10),

                //Good Feel Text
                Text(
                  'Happy to see you!!',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                  ),
                ),

                //Space Between
                SizedBox(height: 30),

                //Email TextField
                TextFClass(
                    getText: _emailController,
                    obscureText: false,
                    textDescript: "Email Address"),

                //Space Between
                SizedBox(height: 30),

                //Password TextField

                TextFClass(
                  getText: _passwordController,
                  obscureText: false,
                  textDescript: 'Password',
                ),

                //Space between
                SizedBox(height: 30),

                //Signin Button
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: GestureDetector(
                    onTap: signIn,
                    child: Container(
                      child: Text(
                        'Sign In',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18),
                      ),
                      color: Colors.blueGrey,
                    ),
                  ),
                ),

                //Space between
                SizedBox(height: 16),

                //not a member? Register
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Not a member ?',
                      style: TextStyle(color: Colors.white, fontSize: 17.0),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    button(
                      colorful: Colors.blueGrey,
                      name: 'Register Here',
                      route: Registration(),
                    ),
                  ],
                ),
              ],
            ),
          ))),
        ));
  }
}
