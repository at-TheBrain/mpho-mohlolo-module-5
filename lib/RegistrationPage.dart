import 'package:flutter/material.dart';
import 'package:universe/DashBoard.dart';
import 'TextField.dart';
import 'button.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Registration extends StatelessWidget {
  Registration({Key? key}) : super(key: key);

  final _usernameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confirmpasswordController = TextEditingController();
  final _dateofbirthController = TextEditingController();

  @override
  void dispose() {
    _usernameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _confirmpasswordController.dispose();
    _dateofbirthController.dispose();
  }

//Authenticate user
//create user
  bool passwordConfirmed() {
    if (_passwordController.text.trim() ==
        _confirmpasswordController.text.trim()) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Future registere() async {
    if (passwordConfirmed()) {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: _emailController.text.trim(),
          password: _passwordController.text.trim());

      //add user details
      addUserDetails(
        _usernameController.text.trim(),
        _emailController.text.trim(),
        _dateofbirthController.text.trim(),
      );
    }
  }

  Future addUserDetails(
      String firstName, String Email, String DateOfObirth) async {
    await FirebaseFirestore.instance.collection('users').add({
      'Username': firstName,
      'Email': Email,
      'Date Of Birth': DateOfObirth,
    });

    @override
    Widget build(BuildContext context) {
      // TODO: implement build
      throw UnimplementedError();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.grey,
          title: const Text(
            'Welcome on Board Gazor :)',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 24,
                color: Colors.white,
                fontFamily: 'IndieFlower'),
          ),
        ),
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/Zodiac/try1.gif'),
                  fit: BoxFit.cover)),
          child: Center(
            child: Container(
              child: SingleChildScrollView(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      //Space Betweeen
                      SizedBox(height: 52),

                      //Good Feel Text
                      Text(
                        'Please fill in your details',
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      ),

                      //Space Between
                      SizedBox(height: 45),

                      //username
                      TextFClass(
                          getText: _usernameController,
                          obscureText: false,
                          textDescript: "Username"),
                      SizedBox(height: 15),
                      //Email Address
                      TextFClass(
                          getText: _emailController,
                          obscureText: false,
                          textDescript: "Email Address"),
                      SizedBox(height: 15),

                      //Password
                      TextFClass(
                          getText: _passwordController,
                          obscureText: false,
                          textDescript: "Password"),
                      SizedBox(height: 15),

                      //Confirm Pass
                      TextFClass(
                          getText: _confirmpasswordController,
                          obscureText: false,
                          textDescript: "Confirm Password"),
                      SizedBox(height: 15),

                      //Date of Birth
                      TextFClass(
                          getText: _dateofbirthController,
                          obscureText: false,
                          textDescript: "Date of Birth"),
                      SizedBox(height: 15),

                      //Register Button
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 25.0),
                        child: Container(
                          padding: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              color: Colors.grey,
                              borderRadius: BorderRadius.circular(12)),
                          child: Center(
                            child: button(
                              colorful: Colors.blueGrey,
                              name: 'Register',
                              route: DashBoard(),
                            ),
                          ),
                        ),
                      ),
                    ]),
              ),
            ),
          ),
        ));
  }
}
