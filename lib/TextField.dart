import 'package:flutter/material.dart';

class TextFClass extends StatelessWidget {
  final String textDescript;

  TextEditingController getText;

  TextFClass({
    Key? key,
    required this.getText,
    required this.textDescript,
    required bool obscureText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.grey[200],
            border: Border.all(color: Colors.white),
            borderRadius: BorderRadius.circular(12)),
        child: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: TextField(
            controller: getText,
            decoration: InputDecoration(
                border: InputBorder.none, hintText: textDescript),
          ),
        ),
      ),
    );
  }
}
