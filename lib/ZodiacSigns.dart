import 'package:flutter/material.dart';
import 'signsClass.dart';

class Signs extends StatefulWidget {
  const Signs({Key? key}) : super(key: key);

  @override
  State<Signs> createState() => _SignsState();
}

class _SignsState extends State<Signs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Edit Profile'),
          elevation: 15,
          backgroundColor: Colors.grey,
          centerTitle: true,
        ),
        body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/Zodiac/try2.gif'),
                    fit: BoxFit.cover)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(50),
                ),
                SizedBox(
                  height: 56,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(5, 25, 5, 0),
                  child: Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      height: 315,
                      //color: Colors.blueGrey,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: <Widget>[signClass()],
                      )),
                ),
                SizedBox(
                  height: 6,
                ),
                Container(
                  padding: EdgeInsets.all(50),
                )
              ],
            )));
  }
}
