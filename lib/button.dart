import 'package:flutter/material.dart';

class button extends StatelessWidget {
  const button(
      {Key? key,
      required this.name,
      required this.route,
      required this.colorful})
      : super(key: key);
  final String name;
  final Widget route;
  final Color colorful;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: Container(
        decoration: BoxDecoration(
            color: colorful, borderRadius: BorderRadius.circular(12)),
        child: Center(
          child: RaisedButton(
            elevation: 0,
            child: Text(
              name,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => route),
                  ));
            },
            color: colorful,
          ),
        ),
      ),
    );
  }
}
