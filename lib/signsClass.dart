import 'package:flutter/material.dart';

class signClass extends StatelessWidget {
  const signClass({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        //Picture 1
        Column(
          children: <Widget>[
            Container(
              child: Image.asset('assets/Zodiac/aquarius.png'),
            ),
            Text(
              'Aquarius\n',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.pink,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
        SizedBox(
          width: 12,
        ),

//Picture 2
        Column(
          children: <Widget>[
            Container(
              child: Image.asset('assets/Zodiac/aries.png'),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Aries\n',
              style: TextStyle(
                  fontSize: 30,
                  color: Colors.pink,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
        SizedBox(
          width: 12,
        ),

        //Picture 3
        Column(
          children: <Widget>[
            Container(
              child: Image.asset('assets/Zodiac/cancer.png'),
            ),
            Text(
              'Cancer\n',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.pink,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
        SizedBox(
          width: 12,
        ),

        //Picture 4
        Column(
          children: <Widget>[
            Container(
              child: Image.asset('assets/Zodiac/capri.png'),
            ),
            Text(
              'Capri\n',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.pink,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
        SizedBox(
          width: 12,
        ),

        //Picture 5
        Column(
          children: <Widget>[
            Container(
              child: Image.asset('assets/Zodiac/gemini.png'),
            ),
            Text(
              'Gemini\n',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.pink,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
        SizedBox(
          width: 12,
        ),
        //Picture 6
        Column(
          children: <Widget>[
            Container(
              child: Image.asset('assets/Zodiac/leo.png'),
            ),
            Text(
              'Leo\n',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.pink,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
        SizedBox(
          width: 12,
        ),
        //Picture 7
        Column(
          children: <Widget>[
            Container(
              child: Image.asset('assets/Zodiac/libra.png'),
            ),
            Text(
              'Libra\n',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.pink,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
        SizedBox(
          width: 12,
        ),
        //Picture 18
        Column(
          children: <Widget>[
            Container(
              child: Image.asset('assets/Zodiac/pisces.png'),
            ),
            Text(
              'Pisces\n',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.pink,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
        SizedBox(
          width: 12,
        ),
        //Picture 9
        Column(
          children: <Widget>[
            Container(
              child: Image.asset('assets/Zodiac/sagi.png'),
            ),
            Text(
              'Sagi\n',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.pink,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
        SizedBox(
          width: 12,
        ),
        //Picture 10
        Column(
          children: <Widget>[
            Container(
              child: Image.asset('assets/Zodiac/scopio.png'),
            ),
            Text(
              'Scorpion\n',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.pink,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
        SizedBox(
          width: 12,
        ),
        //Picture 11
        Column(
          children: <Widget>[
            Container(
              child: Image.asset('assets/Zodiac/aquarius.png'),
            ),
            Text(
              'Aquarius\n',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.pink,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
        SizedBox(
          width: 12,
        ),
        //Picture 12
        Column(
          children: <Widget>[
            Container(
              child: Image.asset('assets/Zodiac/virgo.png'),
            ),
            Text(
              'Virgo\n',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.pink,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
        SizedBox(
          width: 12,
        ),
      ],
    );
  }
}
